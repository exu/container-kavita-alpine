# This Dockerfile creates an alpine container for x64 architectures

FROM alpine:3.16.2

# build variables
ARG KAVITA_VERSION="0.5.6"

# use musl package
RUN wget https://github.com/Kareadita/Kavita/releases/download/v${KAVITA_VERSION}/kavita-linux-musl-x64.tar.gz -O kavita-linux-musl-x64.tar.gz

# extract release
RUN tar xf kavita-linux-musl-x64.tar.gz

# rename folder to lowercase
RUN mv /Kavita /kavita

# remove archive
RUN rm kavita-linux-musl-x64.tar.gz

# in /etc/apk/repositories
# enable edge/testing repo (with tag @testing)
# https://wiki.alpinelinux.org/wiki/Package_management#Repository_pinning
RUN echo '@testing https://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories

# install kavita dependencies
# icu
# vips
RUN apk add \
    icu-dev libssl1.1 curl \
    libgdiplus@testing

# get entrypoint script from my repo
RUN wget https://raw.githubusercontent.com/RealStickman/Kavita/rs_alpine/entrypoint.sh -O /kavita/entrypoint.sh
RUN chmod +x /kavita/entrypoint.sh

EXPOSE 5000/tcp

# change working directory to "/kavita"
WORKDIR /kavita

ENTRYPOINT [ "/bin/ash" ]
CMD ["/kavita/entrypoint.sh"]
