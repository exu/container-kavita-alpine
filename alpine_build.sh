#!/bin/sh
set -eu

# make sure location is root
cd /

# use my fork for now
#git clone https://github.com/Kareadita/Kavita.git
git clone https://github.com/RealStickman/Kavita.git

# enter repo
cd Kavita

# use my branch for now
git checkout rs_alpine

# start build for linux-musl-x64
"/Kavita/build.sh" linux-musl-x64

# copy builds to host mounted folder
mkdir -p /output
cp /Kavita/_output/*.tar.gz /output
