# This Dockerfile builds kavita in alpine

FROM alpine:3.16.2

# Install necessary packages
RUN apk add git \
    dotnet6-sdk npm

# copy download & build script
COPY ["alpine_build.sh","/"]

ENTRYPOINT [ "/bin/ash" ]
CMD [ "alpine_build.sh" ] # call build script here

# bind mount "/output" to any host folder
